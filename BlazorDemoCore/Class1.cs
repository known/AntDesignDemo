﻿namespace BlazorDemoCore;

public class Class1
{
    /*
     *    为什么要新建类库项目？
     * 
     * 1. 查询框架是通用的，所有项目都可以使用
     * 2. 将框架封装在类库中，便于其他项目引用
     * 3. 框架代码与项目代码隔离，避免被篡改
     * 
     */
}