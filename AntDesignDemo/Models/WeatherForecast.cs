﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AntDesignDemo.Models;

public class WeatherForecast
{
    [DisplayName("日期")]
    [Required(ErrorMessage = "日期不能为空！")]
    public DateOnly Date { get; set; }
    [DisplayName("温度(C)")]
    [Required(ErrorMessage = "温度不能为空！")]
    public int TemperatureC { get; set; }
    [DisplayName("摘要")]
    public string? Summary { get; set; }
    [DisplayName("温度(F)")]
    public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);
}